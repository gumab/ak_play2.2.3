package controllers

import play.api._
import play.api.mvc._
import play.api.data.Forms._
import play.api.data._
import play.api.libs.json._
import play.api.libs.json.Json._
import models._
//import scala.collection.mutable.ArrayBuffer
import play.api.libs.functional.syntax._
import java.text.SimpleDateFormat
import java.util.Calendar
import scala.io._
import scala.math._


object Application extends Controller {
  
  def index = Action {
    Ok(views.html.index("Your new application is ready."))
    //Redirect("http://www.auction.co.kr")scala using api
  }

  def allkill = Action {
    val allDayUrl = new java.net.URL("http://searchapi.auction.co.kr/allkill/items/allday?site=p")
    val themeUrl = new java.net.URL("http://searchapi.auction.co.kr/allkill/items/theme?site=p")
   //val allDayJson = Source.fromInputStream(allDayUrl.openStream).getLines.mkString("\n")
   //val themeJson = Source.fromInputStream(allDayUrl.openStream).getLines.mkString("\n")
    val temp=new apis
    val themeJson: String=temp.getThemeJson
    val allDayJson: String=temp.getAllDayJson

    //val today=Calendar.getInstance().getTime()
    //val timeformat=new SimpleDateFormat("YYYY-MM-dd'T'hh:mm:ss")
    //val current=timeformat.format(today)
    val SelectedItemNo=0

    val adjson = Json.parse(allDayJson)
    val allDayItems = adjson.as[List[AllKillItem]]
    val thjson = Json.parse(themeJson)
    val themeItems=thjson.as[List[AllKillItem]]
    val items=allDayItems:::themeItems    
    var item_html=""
    val hm= new HtmlMaker
    hm.mkHtml(item_html,items,SelectedItemNo)
    item_html=hm.getHtml

    Ok(views.html.allkill("0101", item_html))

  }



}
