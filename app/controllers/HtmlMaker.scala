package controllers
import models._
import scala.math._

class HtmlMaker {
  var ht=""
  def mkHtml(html:String,items:List[AllKillItem],SelectedItemNo:Int){
    for (i <- 0 to items.length - 1) {
      ht += "<li "
      if (items(i).stockQty == 0) {
        ht += " class=\"disabled\""
      } else {
        if (SelectedItemNo == items(i).itemNo) {
          ht += " class=\"click\""
        }
        ht += " id=\"click_" + items(i).itemNo + "\">"
      }
      ht += " <div class=\"inner\"><div class=\"item_img_type1\">"
      if (items(i).stockQty == 0) {
        ht += " <a class=\"img_box h_type3\" href=\"javascript:;\">"
      } else {
        ht += " <a class=\"img_box h_type3\" href=\"" + items(i).landingUrl + "\" target=\"_blank\">"
      }
      ht += "<img class=\"item_img\" src=\"http://pics.auction.co.kr/p_templ/allkill_noimage.jpg\" data-original=\"" + items(i).imageUrl + "\" alt=\"" + items(i).visibleItemName + "\" /></a><span class=\"icon_area4 \">"
      if (items(i).sellPrice != items(i).discountedPrice && items(i).dcRateVisible == false && floor((items(i).sellPrice - items(i).discountedPrice) / items(i).sellPrice * 100) > 0) {
      //  if(true){
        ht += "<span class=\"sale_percent3\"><b>" + floor((items(i).sellPrice - items(i).discountedPrice) / items(i).sellPrice * 100).toInt + "</b><em>%</em></span>"
      }

      if (items(i).isFreeShipping) {
        ht += "<img src=\"http://pics.auction.co.kr/p_templ/item_list_icon03_big.gif\" alt=\"\" />"
      }
      if (items(i).tagText1 != "" && items(i).tagText1 != null) {
        ht += "<span class=\"days\"><b>" + items(i).tagText1 + "</b></span>"
      }
      if (items(i).tagText2 != "" && items(i).tagText2 != null) {
        ht += "<span class=\"allkill_sale2\">" + items(i).tagText2 + "</span>"
      }
      if (items(i).tagText3 != "" && items(i).tagText3 != null) {
        ht += "<span class=\"allkill_sale\">" + items(i).tagText3 + "</span>"
      }
      ht += "</span>"
      if (items(i).stockQty == 0) {
        ht += "<span class=\"icon_area3 \"><img src=\"http://pics.auction.co.kr/p_templ/item_list_icon02.png\" alt=\"SOLD OUT\" /></span>"
      }
      ht += "</div><div class=\"item_info_type1\"><p class=\"allkill_box\">"
      if (items(i).stockQty == 0) {
        ht += "<a href=\"javascript:;\">" + items(i).serviceText + "</a>"
      } else {
        ht += "<a href=" + items(i).landingUrl + " target=\"_blank\">" + items(i).serviceText + "</a>"
      }
      ht += "<span>" + items(i).visibleItemName + "</span></p><p class=\"price_box\"><span class=\"price_org\">"
      if (items(i).sellPrice != items(i).discountedPrice) {
        ht += "<del>" + items(i).sellPrice.toInt.toString + "</del>"
      }
      ht += "</span><span class=\"price_ing\"><b class=\"s_big\">" + items(i).discountedPrice.toInt.toString + "</b></span></p></div></div></li>"

    }
  }
  
  
  def getHtml: String =ht
}
