package models
case class AllKillItem(
  itemNo: String,
  visibleItemName: String,
  serviceText: String,
  visibleStartDate: String,
  visibleEndDate: String,
  landingUrl: String,
  imageUrl: String,
  bannerImageUrl: String,
  dcRateVisible: Boolean,
  tagText1: String,
  tagText2: String,
  tagText3: String,
  isVisible: Boolean,
  isHomeVisible: Boolean,
  tabSeqNo: Int,
  visibleOrderBy: Int,
  sellPrice: Double,
  discountedPrice: Double,
  isFreeShipping: Boolean,
  stockQty: Int)

object AllKillItem {
  import play.api.libs.json._
  implicit object ItemFormat extends Format[AllKillItem] {

    def reads(json: JsValue): JsResult[AllKillItem] = {
      val itemNo = (json \ "ItemNo").as[String]
      val visibleItemName = (json \ "VisibleItemName").as[String]
      val serviceText = (json \ "ServiceText").as[String]
      val visibleStartDate = (json \ "VisibleStartDate").as[String]
      val visibleEndDate = (json \ "VisibleEndDate").as[String]
      val landingUrl = (json \ "LandingUrl").as[String]
      val imageUrl = (json \ "ImageUrl").as[String]
      val bannerImageUrl = (json \ "BannerImageUrl").as[String]
      val dcRateVisible = (json \ "DCRateVisible").as[Boolean]
      val tagText1 = (json \ "TagText1").as[String]
      val tagText2 = (json \ "TagText2").as[String]
      val tagText3 = (json \ "TagText3").as[String]
      val isVisible = (json \ "IsVisible").as[Boolean]
      val isHomeVisible = (json \ "IsHomeVisible").as[Boolean]
      val tabSeqNo = (json \ "TabSeqNo").as[Int]
      val visibleOrderBy = (json \ "VisibleOrderBy").as[Int]
      val sellPrice = (json \ "SellPrice").as[Double]
      val discountedPrice = (json \ "DiscountedPrice").as[Double]
      val isFreeShipping = (json \ "IsFreeShipping").as[Boolean]
      val stockQty = (json \ "StockQty").as[Int]

      JsSuccess(AllKillItem(itemNo
        , visibleItemName
        , serviceText
        , visibleStartDate
        , visibleEndDate
        , landingUrl
        , imageUrl
        , bannerImageUrl
        , dcRateVisible
        , tagText1
        , tagText2
        , tagText3
        , isVisible
        , isHomeVisible
        , tabSeqNo
        , visibleOrderBy
        , sellPrice
        , discountedPrice
        , isFreeShipping
        , stockQty))
    }
    def writes(n: AllKillItem): JsValue = {
      val itemAsList = Seq("ItemNo" -> JsString(n.itemNo),
        "VisibleItemName" -> JsString(n.visibleItemName))
      JsObject(itemAsList)
    }
  }
}
